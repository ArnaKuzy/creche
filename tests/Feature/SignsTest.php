<?php

namespace Tests\Feature;

use App\Baby;
use App\Family;
use App\Sign;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SignsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testBabySign()
    {
        $family = factory(Family::class)->create();
        $baby = $family->babies()->save(factory(Baby::class)->make());

        $response = $this->put('/sign/' . $family->uuid, [
            'date' => '2020-01-01',
            'sign' => [$baby->id => 'on'],
            'in' => [$baby->id => '08:35:32']
        ]);

        $response->assertStatus(302);
        $this->assertCount(1, $baby->signs);
    }

    public function testBabiesSign()
    {
        $family = factory(Family::class)->create();
        $babies = $family->babies()->createMany(factory(Baby::class, 2)->make()->toArray());

        $response = $this->put('/sign/' . $family->uuid, [
            'date' => '2020-01-01',
            'sign' => [
                $babies[0]->id => 'on',
                $babies[1]->id => 'on'
            ],
            'in' => [
                $babies[0]->id => '08:35:32',
                $babies[1]->id => '08:35:32'
            ]
        ]);

        $response->assertStatus(302);
        $this->assertCount(1, $babies[0]->signs);
        $this->assertCount(1, $babies[1]->signs);
    }

    public function testSignOut()
    {
        $family = factory(Family::class)->create();
        $baby = $family->babies()->save(factory(Baby::class)->make());
        $sign = $baby->signs()->save(factory(Sign::class)->make(['out' => null]));

        $response = $this->put('/sign/' . $family->uuid, [
            'date' => $sign->sign_date,
            'sign' => [$baby->id => 'on',],
            'in' => [$baby->id => $sign->in],
            'out' => [$baby->id => '18:30:00']
        ]);

        $response->assertStatus(302);
        $this->assertEquals('18:30:00', Sign::find($sign->id)->out, $sign);
    }
}
