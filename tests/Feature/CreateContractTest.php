<?php

namespace Tests\Feature;

use App\Family;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateContractTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUniqueUUID()
    {
        $families = factory(Family::class, 2)->create();

        $response = $this->post('/contract', [
            'lastname' => $families[0]->lastname,
            'firstname' => 'John',
            'birth_date' => '2021-06-06',
            'expected_in' => 0,
            'expected_out' => 0
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('uuid', $families[0]->uuid);

        $response = $this->post('/contract', [
            'lastname' => $families[1]->lastname,
            'firstname' => 'John',
            'birth_date' => '2021-06-06',
            'expected_in' => 0,
            'expected_out' => 0
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('uuid', $families[1]->uuid);
    }
}
