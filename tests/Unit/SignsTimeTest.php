<?php

namespace Tests\Unit;

use App\Baby;
use App\Sign;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SignsTimeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testTimeInExpected()
    {
        $baby = factory(Baby::class)->create([
            'family_id' => 0,
            'expected_in' => '08:30:00',
            'expected_out' => '18:00:00'
        ]);
        $sign = [
            factory(Sign::class)->make([
                'baby_id' => $baby->id,
                'in' => '08:30:00',
                'out' => '18:00:00'
            ]),
            factory(Sign::class)->make([
                'baby_id' => $baby->id,
                'in' => '09:00:00',
                'out' => '17:00:00'
            ]),
            factory(Sign::class)->make([
                'baby_id' => $baby->id,
                'in' => '08:30:00',
                'out' => '18:29:00'
            ]),
        ];

        $this->assertEquals($sign[0]->timeOver, null);
        $this->assertEquals($sign[1]->timeOver, null);
        $this->assertEquals($sign[2]->timeOver, null);
    }

    public function testTimeOverExpected()
    {
        $baby = factory(Baby::class)->create([
            'family_id' => 0,
            'expected_in' => '08:30:00',
            'expected_out' => '18:00:00'
        ]);
        $sign = [
            factory(Sign::class)->make([
                'baby_id' => $baby->id,
                'in' => '08:00:00',
                'out' => '18:00:00'
            ]),
            factory(Sign::class)->make([
                'baby_id' => $baby->id,
                'in' => '08:30:00',
                'out' => '18:30:00'
            ]),
            factory(Sign::class)->make([
                'baby_id' => $baby->id,
                'in' => '08:30:00',
                'out' => '18:35:00'
            ]),
            factory(Sign::class)->make([
                'baby_id' => $baby->id,
                'in' => '08:00:00',
                'out' => '18:30:00'
            ])
        ];

        $this->assertEquals($sign[0]->timeOver, '+00h30');
        $this->assertEquals($sign[1]->timeOver, '+00h30');
        $this->assertEquals($sign[2]->timeOver, '+00h30');
        $this->assertEquals($sign[3]->timeOver, '+01h00');
        $this->assertTrue(true);
    }
}
