<?php

namespace App;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $fillable = [
        'uuid',
        'lastname'
    ];

    /**
     * Get the babies of the family
     */
    public function babies()
    {
        return $this->hasMany('App\Baby');
    }

    public static function genUUID(Faker $faker)
    {
        $uuids = Family::pluck('uuid');

        do
            $uuid = $faker->unique()->bothify('##???');
        while ($uuids->contains($uuid));

        return $uuid;
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
