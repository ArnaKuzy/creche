<?php

namespace App\Http\Controllers;

use App\Http\Requests\Login;

class LoginController extends Controller
{
    public function __invoke(Login $request)
    {
        $data = $request->validated();

        return redirect()->route($request->get('route'), $data['uuid']);
    }
}
