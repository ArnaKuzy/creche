<?php

namespace App\Http\Controllers;

use App\Baby;
use App\Family;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    public function create()
    {
        return view('contract.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'lastname' => 'required|string',
            'firstname' => 'required|string',
            'birth_date' => 'required|date',
            'expected_in' => 'required',
            'expected_out' => 'required',
        ]);

        $family = Family::where('lastname', $data['lastname'])->get()->first();

        if (is_null($family)) {
            $family = Family::create([
                'uuid' => Family::genUUID(\Faker\Factory::create()),
                'lastname' => $data['lastname']
            ]);
        }

        $baby = $family->babies()->create($data);

        return redirect()->route('contract.create')->with([
            'status' => 'Nouveau contrat créé',
            'uuid' => $baby->family->uuid
        ]);
    }
}
