<?php

namespace App\Http\Controllers;

use App\Baby;
use App\Family;
use App\Sign;
use Illuminate\Http\Request;

class SignController extends Controller
{
    public function show(Family $family)
    {
        return view('sign.show', [
            'family' => $family,
            'current_time' => date('H:i:s')
        ]);
    }

    public function update(Request $request, Family $family)
    {
        $data = $request->validate([
            'date' => 'required|date',
            'sign' => 'required|array',
            'in' => 'array',
            'out' => 'array'
        ]);

        foreach ($data['sign'] as $id => $value) {
            $baby = Baby::find($id);
            $sign = Sign::where([
                ['baby_id', $baby->id],
                ['sign_date', $data['date']]
            ])
                ->get()->first();

            if (is_null($sign))
                $baby->signs()->create([
                    'sign_date' => $data['date'],
                    'in' => $data['in'][$id]
                ]);
            else {
                $sign->in = $data['in'][$id];
                if (isset($data['out'][$id]))
                    $sign->out = $data['out'][$id];
                $sign->save();
            }
        }

        return redirect()->route('sign.show', $family->uuid)->with('status', 'Sign updated');
    }
}
