<?php

namespace App\Http\Controllers;

use App\Family;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SummaryController extends Controller
{
    public function index()
    {
        return view('summary.index');
    }

    public function show(Request $request, Family $family)
    {
        $date = ($request->has('period')) ? Carbon::create($request->get('period')) : Carbon::now();
        $prev_period = $date->copy()->subMonthNoOverflow()->format('Y-m-d');
        $next_period = $date->copy()->addMonthNoOverflow()->format('Y-m-d');

        return view('summary.show', [
            'family' => $family,
            'period' => $date,
            'prev_period' => $prev_period,
            'next_period' => $next_period
        ]);
    }
}
