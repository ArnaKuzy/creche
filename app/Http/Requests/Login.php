<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Login extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => 'required|exists:families,uuid|size:5'
        ];
    }

    public function messages()
    {
        return [
            'uuid.required' => "L'identifiant est requis !",
            'uuid.exists' => "L'identifiant n'existe pas !",
            'uuid.size' => "L'identifiant n'existe pas !",
        ];
    }
}
