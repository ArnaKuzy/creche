<?php

namespace App\Http\Middleware;

use App\Family;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
            !$request->has('uuid') &&
            Family::where('uuid', $request->get('uuid'))->get()->isEmpty()
        )
            return redirect()->to('home');

        return $next($request);
    }
}
