<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Baby extends Model
{
    protected $fillable = [
        'firstname',
        'birth_date',
        'expected_in',
        'expected_out'
    ];

    /**
     * Get the baby family
     */
    public function family()
    {
        return $this->belongsTo('App\Family');
    }

    /**
     * Get the signs of the baby
     */
    public function signs()
    {
        return $this->hasMany('App\Sign');
    }

    public function signsByMonth(Carbon $date)
    {
        $date = $date->copy();
        $date->tz('Europe/Paris');
        $year = $date->year;
        $month = $date->month;

        $stored_signs = $this->signs()->where('sign_date', 'like', "$year-%$month-%")->get()->sortByDesc('sign_date');
        $signs = [];
        $last = $date->copy()->lastOfMonth();

        for ($day = $date->firstOfMonth(); $day->lessThanOrEqualTo($last); $day->addDay()) {
            $sign = $stored_signs->firstWhere('sign_date', $day->format('Y-m-d'));

            $signs[] = ($sign) ?
                $sign :
                new Sign([
                    'sign_date' => $day->copy(),
                    'in' => 0,
                    'out' => 0
                ]);
        }

        return $signs;
    }

    /**
     * Get the today signs of the baby
     */
    public function todaySigns()
    {
        return $this->signs()->where('sign_date', date('Y-m-d'))->get()->first();
    }

    public function getFullNameAttribute()
    {
        return $this->family->lastname . ' ' . $this->firstname;
    }

    public function getAgeAttribute()
    {
        $now = new Carbon('now', 'Europe/Paris');
        $date = new Carbon($this->birth_date, 'Europe/Paris');

        $years = $now->diffInYears($date);
        $months = $now->diffInMonths($date->addYears($years));

        $fullAge = ($years > 0) ? $years . ' an' : '';
        $fullAge .= ($years > 1) ? 's' : '';
        $fullAge .= ($months > 0) ? ' ' . $months . ' mois' : '';

        return $fullAge;
    }

    public function getExpectedInFormatAttribute()
    {
        return (new Carbon($this->expected_in, 'Europe/Paris'))->format('H\hi');
    }

    public function getExpectedOutFormatAttribute()
    {
        return (new Carbon($this->expected_out, 'Europe/Paris'))->format('H\hi');
    }
}
