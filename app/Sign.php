<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sign extends Model
{
    public $timestamps = false;

    public $fillable = [
        'sign_date',
        'in'
    ];

    public function baby()
    {
        return $this->belongsTo('App\Baby');
    }

    public function getDateAttribute()
    {
        return (new Carbon($this->sign_date, 'Europe/Paris'))->format('d m Y');
    }

    public function getTimeInAttribute()
    {
        return (new Carbon($this->in, 'Europe/Paris'))->format('H\hi');
    }

    public function getTimeOutAttribute()
    {
        return ($this->out) ?
            (new Carbon($this->out, 'Europe/Paris'))->format('H\hi') :
            '';
    }

    public function getTimeOverAttribute(): ?String
    {
        // If not present
        if ($this->in == 0)
            return null;

        $baby = $this->baby;
        $in_diff = ((new Carbon($baby->expected_in, 'Europe/Paris')))
            ->diffInMinutes(new Carbon($this->in, 'Europe/Paris'), false);
        $out_diff = (new Carbon($this->out, 'Europe/Paris'))
            ->diffInMinutes((new Carbon($baby->expected_out, 'Europe/Paris')), false);

        // If over expected
        if ($in_diff < 0 || $out_diff < 0) {
            if ($in_diff < 0)
                $in_diff = -$in_diff;
            if ($out_diff < 0)
                $out_diff = -$out_diff;

            $time = $in_diff + $out_diff;
            if ($time < 30)
                return null;

            $hours = floor($time / 60);
            $minutes = $time - ($hours * 60);

            $minutes = ($minutes < 30) ? 0 : 30;

            return Carbon::create(0, 0, 0, $hours, $minutes)->format('+H\hi');
        }

        return null;
    }
}
