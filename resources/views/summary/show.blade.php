@extends('layouts.public')

@section('body')
    <main class="container">
        <h1>Récapitulatif</h1>
        <h2>Famille : {{ $family->lastname }}</h2>
        <h3>Période : {{ $period->startOfMonth()->format('d-m-Y') }} au {{ $period->endOfMonth()->format('d-m-Y') }}
        </h3>

        <a href="{{ route('summary.show', ['family' => $family->uuid, 'period' => $prev_period]) }}"
            class="btn btn-info m-3">Précédent</a>
        @if (is_null($next_period))
            <div class="btn btn-grey">Suivant</div>
        @else
            <a href="{{ route('summary.show', ['family' => $family->uuid, 'period' => $next_period]) }}"
                class="btn btn-info m-3">Suivant</a>
        @endif

        @foreach ($family->babies as $baby)
            <div class="card mb-2">
                <div class="card-body">
                    <div class="mb-2">
                        <span class="h4">{{ $baby->fullname }}</span>
                        <span class="h5 px-3">{{ $baby->age }}</span>
                        <span class="px-5">Heure d'arrivée estimée : {{ $baby->expected_in_format }}</span>
                        <span class="px-5">Heure de départ estimée : {{ $baby->expected_out_format }}</span>
                    </div>

                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Heure d'arrivée</th>
                                <th>Heure de départ</th>
                                <th>Temps suplémentaire</th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($baby->signsByMonth($period) as $sign)
                                <tr>
                                    <td>{{ $sign->date }}</td>
                                    @if ($sign->timeIn == '00h00')
                                        <td colspan="2">Absent</td>
                                    @else
                                        <td>{{ $sign->timeIn }}</td>
                                        <td>{{ $sign->timeOut }}</td>
                                    @endif
                                    <td>{{ $sign->timeOver }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">Aucune donnée</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
    </main>
@endsection
