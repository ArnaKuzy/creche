@extends('layouts.public')

@section('body')
    @include('views.login', [ 'route' => 'summary.show' ])
@endsection
