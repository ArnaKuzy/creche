<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('app_name') }}</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    <script src="{{ mix('/js/app.js') }}"></script>
</head>

<body>
    <nav class="container navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/">{{ env('app_name') }}</a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('home') }}">Accueil</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('contract.create') }}">Nouveau contrat</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('summary.index') }}">Récapitulatif</a>
                </li>
            </ul>
        </div>
    </nav>

    @yield('body')
</body>

</html>
