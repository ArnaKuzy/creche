@extends('layouts.public')

@section('body')
    <main class="container">
        <div class="h2">Identifiant : {{ $family->uuid }}</div>
        <div class="h3">Date : {{ date('d m Y') }}</div>

        <form action="" method="POST" id="signForm" class="mt-5">
            @csrf
            @method('PUT')

            <input type="hidden" name="date" value="{{ date('Y-m-d') }}">

            @foreach ($family->babies as $baby)
                @php
                    $sign = $baby->todaySigns();
                @endphp

                <input type="hidden" name="name[{{ $baby->id }}]" value="{{ $baby->fullname }}">

                <div class="card mb-2">
                    <div class="pt-2 pl-4 row">
                        <input type="checkbox" name="sign[{{ $baby->id }}]" class="form-control-sm col-1 mt-5"
                            {{ ($sign && $sign->out != null) ? 'disabled' : 'checked' }}>

                        <div class="col">
                            <div class="mb-2">
                                <span class="h4">{{ $baby->fullname }}</span>
                                <h5>{{ $baby->age }}</h5>
                            </div>

                            <div class="row">
                                <div class="form-group col">
                                    <label for="in[{{ $baby->id }}]" class="h5">Heure d'arrivé</label>
                                    <input type="time" name="in[{{ $baby->id }}]" id="in[{{ $baby->id }}]"
                                        class="form-control w-50"
                                        value="{{ is_null($sign) ? $current_time : $sign->in }}"
                                        {{ ($sign && $sign->in) ? 'readonly' : '' }}
                                        >
                                </div>

                                <div class="form-group col">
                                    @if (!is_null($sign))
                                        <label for="out[{{ $baby->id }}]" class="h5">Heure de départ</label>
                                        <input type="time" name="out[{{ $baby->id }}]" id="out[{{ $baby->id }}]"
                                            class="form-control w-50"
                                            value="{{ is_null($sign->out) ? $current_time : $sign->out }}" readonly>
                                    @endif
                                </div>

                                <div class="col h1 {{ is_null($sign) ? 'text-success' : 'text-danger' }}">
                                    {{ is_null($sign) ? 'Arrivée' : ($sign->out != null ? 'Partit' : 'Départ') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            {{-- <button type="button" class="btn btn-primary btn-confirmSigns" data-toggle="modal" data-target="#confirmSigns">Émarger</button> --}}
            <button type="submit" id="btn-confirmSigns" class="btn btn-primary">Émarger</button>

            <!-- Modal -->
            <div class="modal fade" id="confirmSigns" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalScrollableTitle">Confirmer l'émargement ?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-primary" id="btn-confirm">Confirmer</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </main>
@endsection
