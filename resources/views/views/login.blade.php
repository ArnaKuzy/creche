<form action="{{ route('login') }}" method="POST" class="mt-5 text-center w-25 mx-auto">
    @csrf

    <input type="hidden" name="route" value="{{ $route ?? 'home' }}">

    <div class="form-group">
        @error('uuid')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <label for="uuid" class="h2">Identifiant</label>
        <input type="text" name="uuid" id="uuid" class="form-control" placeholder="01ABC" value="{{ old('uuid') }}">

    </div>

    <button type="submit" class="btn btn-primary">S'identifier</button>
</form>
