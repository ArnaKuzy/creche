@extends('layouts.public')

@section('body')
    <main class="container">
        <h1 class="title mx-auto mt-5">{{ env('app_name') }}</h1>

        @include('views.login', [ 'route' => 'sign.show' ])
    </main>
@endsection
