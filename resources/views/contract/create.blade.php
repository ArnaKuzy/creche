@extends('layouts.public')

@section('body')
    <main class="container">
        <h1 class="h1">Nouveau contrat</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('status'))
            <div class="alert alert-success text-center w-25 mx-auto">
                {{ session('status') }}<br>
                Son identifiant est : {{ session('uuid') }}
            </div>
        @endif

        <form action="{{ route('contract.store') }}" method="POST">
            @csrf

            <div class="row mt-5">
                <div class="form-group col">
                    <label for="lastname" class="h5">Nom</label>
                    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Doe" value="Doe">
                </div>

                <div class="form-group col">
                    <label for="firstname" class="h5">Prénom</label>
                    <input type="text" name="firstname" id="firstname" class="form-control" placeholder="Jhon" value="Jhon">
                </div>

                <div class="form-group col">
                    <label for="birth_date" class="h5">Date de naissance</label>
                    <input type="date" name="birth_date" id="birth_date" class="form-control" value="2020-05-14">
                </div>
            </div>

            <div class="row mt-5">
                <div class="col"></div>

                <div class="form-group col">
                    <label for="expected_in" class="h5">Heure d'arrivée estimée</label>
                    <input type="time" name="expected_in" id="expected_in" class="form-control" value="08:30:00">
                </div>

                <div class="form-group col">
                    <label for="expected_out" class="h5">Heure de départ estimée</label>
                    <input type="time" name="expected_out" id="expected_out" class="form-control" value="18:30:00">
                </div>

                <div class="col"></div>
            </div>

            <div class="mt-5 text-center">
                <button type="submit" class="btn btn-success">Créer le contrat</button>
            </div>
        </form>
    </main>
@endsection
