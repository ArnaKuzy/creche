require('./bootstrap');

function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_object = { out_nbr: 0 };

    console.log(unindexed_array)

    $.map(unindexed_array, function (n, i) {
        const key = n['name'].split(/\[(\d)+/)

        if (key.length > 1) {
            if (!(key[1] in indexed_object))
                indexed_object[key[1]] = {}
            indexed_object[key[1]][key[0]] = n['value']
        }
    });

    for (const [key, value] of Object.entries(indexed_object))
        if (key != 'out_nbr' && 'sign' in value)
            indexed_object.out_nbr++

    return indexed_object;
}

$(() => {
    var canSubmit = false

    $('#btn-confirm').on('click', () => {
        canSubmit = true
    })

    $('#signForm').on('submit', (e) => {
        if (!canSubmit)
            e.preventDefault()

        const dest = $('#confirmSigns .modal-body')
        var data = getFormData($('#signForm'));

        console.log(data)

        $('#confirmSigns .modal-body').text('')
        $('#btn-confirm').show()

        if (data.out_nbr == 0) {
            dest.html('<h3 class="h3 text-danger">Vous devez sélectionner au moins 1 bébé</h3>')
            $('#btn-confirm').hide()
        } else
            for (const [key, value] of Object.entries(data))
                if (key != 'out_nbr' && 'sign' in value) {
                    let text = value.name

                    text += ('out' in value) ?
                        ' <span class="text-danger">Départ</span> à ' + value.out :
                        ' <span class="text-success">Arrivée</span> à ' + value.in

                    dest.append('<h3 class="h3">' + text + '</h3>')
                }

        $('#confirmSigns').modal('show')
    })
})
