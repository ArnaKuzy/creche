<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'home')->name('home');
Route::post('/', 'LoginController')->name('login');

Route::get('/sign/{family}', 'SignController@show')->name('sign.show');
Route::put('/sign/{family}', 'SignController@update')->name('sign.update');

Route::get('/contract', 'ContractController@create')->name('contract.create');
Route::post('/contract', 'ContractController@store')->name('contract.store');

Route::get('/summary', 'SummaryController@index')->name('summary.index');
Route::get('/summary/{family}', 'SummaryController@show')->name('summary.show');

// TODO: Fix le middleware
Route::middleware(['auth'])->group(function () {
});
