<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Family;
use Faker\Generator as Faker;

$factory->define(Family::class, function (Faker $faker) {
    return [
        'uuid' => Family::genUUID($faker),
        'lastname' => $faker->lastName(),
    ];
});
