<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Sign;
use Faker\Generator as Faker;

$factory->define(Sign::class, function (Faker $faker) {
    return [
        // 'baby_id' => factory(Baby::class),
        'sign_date' => $faker->unique()->dateTimeThisYear('now', 'Europe/Paris'),
        'in' => date('H:i:s', rand(21600, 43200)), // 6h -> 12h
        'out' => date('H:i:s', rand(43200, 79200)), // 12h -> 22h
    ];
});
