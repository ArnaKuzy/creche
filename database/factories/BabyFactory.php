<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Baby;
use Faker\Generator as Faker;

$factory->define(Baby::class, function (Faker $faker) {
    return [
        // 'family_id' => factory(Family::class),
        'firstname' => $faker->firstName(),
        'birth_date' => $faker->dateTimeBetween('-3 years', 'now', 'Europe/Paris'),
        'expected_in' => date('H:i:s', rand(21600, 43200)), // 6h -> 12h
        'expected_out' => date('H:i:s', rand(43200, 79200)), // 12h -> 22h
    ];
});
