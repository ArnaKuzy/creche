<?php

use App\Baby;
use App\Family;
use App\Sign;
use Illuminate\Database\Seeder;

class FamilyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Family::class, 50)
            ->create()
            ->each(function ($family) {
                $family
                    ->babies()
                    ->createMany(
                        factory(Baby::class, rand(1, 5))
                            ->make()
                            ->toArray()
                    )
                    ->each(function ($baby) {
                        $baby
                            ->signs()
                            ->createMany(
                                factory(Sign::class, rand(0, 50))
                                    ->make()
                                    ->toArray()
                            );
                    });
            });
    }
}
